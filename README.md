# https://darrenbratten.gitlab.io/digital-fabrication_2022/

# My Bio

## Introduction :smiley:

Hi! My name is Darren Bratten and I’m a second year MA student from the *Collaborative & Industrial Design* course at Aalto University, Finland. 

## Purpose of repository 
Here I will update my website (as linked above) with documentation and images for digitial fabrication & Fab academy. 

# Contact details 

## How to get in touch 

:email: **Send me an email:**.    
darren.bratten(at)aalto.fi

:link: **Connect with me on Linkedin:**.  
https://www.linkedin.com/in/dbratten/






