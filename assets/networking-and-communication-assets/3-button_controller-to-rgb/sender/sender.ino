
int button1 = 2;
int button2 = 15;
int button3 = 12;

boolean lastButton1 = LOW;
boolean currentButton1 = LOW;
boolean condition1 = false;

boolean lastButton2 = LOW;
boolean currentButton2 = LOW;
boolean condition2 = false;

boolean lastButton3 = LOW;
boolean currentButton3 = LOW;
boolean condition3 = false;

byte red_incoming_off = 0b00110001;
byte red_incoming_on = 0b00110101;
byte green_incoming_off = 0b00110010;
byte green_incoming_on = 0b00110110;
byte blue_incoming_off = 0b00110011;
byte blue_incoming_on = 0b00110111;

int counter1 = 0;
int counter2 = 0;
int counter3 = 0;


void setup() {

  pinMode (button1, INPUT_PULLUP);
  pinMode (button2, INPUT_PULLUP);
  pinMode (button3, INPUT_PULLUP);


  Serial.begin (9600);


}

void loop() {


  // Define the meaning of 'current' - LOW = LOW?
  currentButton1 = debounce1 (lastButton1);

  currentButton2 = debounce2 (lastButton3);

  currentButton3 = debounce3 (lastButton3);




  // Within the timeframe of this sketch
  // Has the condition changed?
  if (lastButton1 == LOW && currentButton1 == HIGH) {

    condition1 = !condition1; // 0 becomes 1 when something changes

    counter1 = condition1;

    if (counter1 == 0)
    {
      Serial.write(red_incoming_off);
    }
    else if (counter1 == 1)
    {
      Serial.write(red_incoming_on);
    }

  }

  if (lastButton2 == LOW && currentButton2 == HIGH) {

    condition2 = !condition2; // 0 becomes 1 when something changes

    counter2 = condition2;

    if (counter2 == 0)
    {
      Serial.write(green_incoming_off);
    }
    else if (counter2 == 1)
    {
      Serial.write(green_incoming_on);
    }

  }

  if (lastButton3 == LOW && currentButton3 == HIGH) {

    condition3 = !condition3; // 0 becomes 1 when something changes

    counter3 = condition3;

    if (counter3 == 0)
    {
      Serial.write(blue_incoming_off);
    }
    else if (counter3 == 1)
    {
      Serial.write(blue_incoming_on);
    }

  }





  lastButton1 = currentButton1;
  lastButton2 = currentButton2;
  lastButton3 = currentButton3;






}






boolean debounce1 (boolean last1) {


  boolean current1 = digitalRead (button1);
  // i.e. current is returning HIGH

  // if last is opposite and false i.e. returning LOW
  // we know current is reading atfer 5seconds
  // i.e. low is not reading after 5 seconds
  if (last1 != current1) {
    delay (5); // not released
    current1 = digitalRead (button1);
  }


  // return current into last when last is not current
  return current1;

}

boolean debounce2 (boolean last2) {


  boolean current2 = digitalRead (button2);
  // i.e. current is returning HIGH

  // if last is opposite and false i.e. returning LOW
  // we know current is reading atfer 5seconds
  // i.e. low is not reading after 5 seconds
  if (last2 != current2) {
    delay (5); // not released
    current2 = digitalRead (button2);
  }


  // return current into last when last is not current
  return current2;

}


boolean debounce3 (boolean last3) {


  boolean current3 = digitalRead (button3);
  // i.e. current is returning HIGH

  // if last is opposite and false i.e. returning LOW
  // we know current is reading atfer 5seconds
  // i.e. low is not reading after 5 seconds
  if (last3 != current3) {
    delay (5); // not released
    current3 = digitalRead (button3);
  }


  // return current into last when last is not current
  return current3;

}
