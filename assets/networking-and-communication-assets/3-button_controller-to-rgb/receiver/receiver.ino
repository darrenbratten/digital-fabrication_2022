
char incoming; 

#define BLUE 4
#define GREEN 3
#define RED 2


byte inputBitmask = 0b00000011; 
byte outputBitmask = 0b00001100; 

#define ON 0b00000001
#define OFF 0b00000000

byte myInputADDR;
byte myOutputADDR;



#define greenDeviceADDR 0b00000001 // 1 --->  1 + 5
#define redDeviceADDR 0b00000010 // 10 --->   2 + 6
#define blueDeviceADDR 0b00000011// 00 --->   3 + 7


void setup() {
  // put your setup code here, to run once:

  Serial.begin (9600); 

    pinMode (BLUE, OUTPUT);
  pinMode (GREEN, OUTPUT);
  pinMode (RED, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:

  while (Serial.available ()) {

    incoming = Serial.read(); 

    myInputADDR = (incoming & inputBitmask); 
    myOutputADDR = (incoming & outputBitmask) >> 2;
    
    if (myInputADDR == greenDeviceADDR && myOutputADDR == ON) {
          Serial.println ("Green ON"); 
          Serial.print ("Incoming Byte:");  
          Serial.print (incoming, BIN); 
          Serial.print ("<-----myinPut address"); 
    Serial.println (); 

    }
    if (myInputADDR == greenDeviceADDR && myOutputADDR == OFF) {
          Serial.println ("Green OFF"); 
           Serial.print ("Incoming Byte:");  
          Serial.print (incoming, BIN); 
          Serial.print ("<-----myinPut address"); 
    Serial.println (); 

    }
    if (myInputADDR == redDeviceADDR && myOutputADDR == ON) {
          Serial.println ("Red ON"); 
           Serial.print ("Incoming Byte:");  
       Serial.print (incoming, BIN); 
       Serial.print ("<-----myinPut address"); 
    Serial.println (); 

    }
    if (myInputADDR == redDeviceADDR && myOutputADDR == OFF) {
          Serial.println ("Red OFF"); 
           Serial.print ("Incoming Byte:");  
          Serial.print (incoming, BIN); 
          Serial.print ("<-----myinPut address"); 
    Serial.println (); 

    }if (myInputADDR == blueDeviceADDR && myOutputADDR == ON) {
          Serial.println ("Blue ON"); 
           Serial.print ("Incoming Byte:");  
      Serial.print (incoming, BIN); 
      Serial.print ("<-----myinPut address"); 
    Serial.println (); 

    }
    if (myInputADDR == blueDeviceADDR && myOutputADDR == OFF) {
          Serial.println ("Blue OFF"); 
           Serial.print ("Incoming Byte:");  
       Serial.print (incoming, BIN); 
       Serial.print ("<-----myinPut address"); 
    Serial.println (); 

    }



    
  }

  
 if (myOutputADDR == OFF)
  {
    mode_is_off (); // function
  
  }

  if (myOutputADDR == ON)  {
    mode_is_on (); // function
  }


}

void mode_is_off () {


 if (myInputADDR == blueDeviceADDR) {
    int devicePin = BLUE;
    digitalWrite (devicePin, HIGH);
  }

   if (myInputADDR == greenDeviceADDR) {
    int devicePin = GREEN;
    digitalWrite (devicePin, HIGH);
  }

 if (myInputADDR == redDeviceADDR) {
    int devicePin = RED;
    digitalWrite (devicePin, HIGH);
  }

}


void mode_is_on () {


 if (myInputADDR == blueDeviceADDR) {
    int devicePin = BLUE;
    digitalWrite (devicePin, LOW);
  }

   if (myInputADDR == greenDeviceADDR) {
    int devicePin = GREEN;
    digitalWrite (devicePin, LOW);
  }

 if (myInputADDR == redDeviceADDR) {
    int devicePin = RED;
    digitalWrite (devicePin, LOW);
  }

}
