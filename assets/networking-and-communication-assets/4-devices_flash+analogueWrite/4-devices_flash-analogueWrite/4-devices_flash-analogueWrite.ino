


// ------------------------------------
// OUTPUT PINS
#define BLUE 3
#define GREEN 5
#define RED 9
#define WHITE 10


// OUTPUT MODES
#define OFF 0b00000000
#define FLASH_TWO 0b00000001
#define FADE 0b00000010

// DEVICE INPUT ADDRESSES
#define blueDeviceADDR 0b00000000// 00 --->   0 + 4 + 8
#define greenDeviceADDR 0b00000001 // 1 --->  1 + 5 + 9
#define redDeviceADDR 0b00000010 // 10 --->   2 + 6 + :
#define whiteDeviceADDR 0b00000011 // 11 ---> 3 + 7 + ;

// Bit masks
byte inputMask =  0b00000011;
byte outputMask = 0b00001100;

// Global variables
byte myInputADDR;
byte myOutputADDR;
char incoming;



void setup() {

  pinMode (BLUE, OUTPUT);
  pinMode (GREEN, OUTPUT);
  pinMode (RED, OUTPUT);
  pinMode (WHITE, OUTPUT);

  Serial.begin (9600); // opening serial port


}

void loop() {

// Open the buffer
  while (Serial.available ()) {

    // Use Serial.read() to read incoming characters from the monitor
    // call the reading 'incoming'
    incoming = Serial.read();
    
 

    // Call the incoming "my input address" and get address with an AND bitwise operation
    myInputADDR = incoming & inputMask;

    // Also call the incoming "my output address" with an AND and shift right bitwise operation
    myOutputADDR = (incoming & outputMask) >> 2;


 if (myOutputADDR == OFF)
  {
   
    Serial.println("OFF");
    Serial.print (myOutputADDR, BIN);
    Serial.print ("<---my output address");
     Serial.println(); 
    Serial.println(); 
  }


 if (myOutputADDR == FLASH_TWO)  {
 
    Serial.println("FLASH TWO");
    Serial.print (myOutputADDR, BIN);
    Serial.print ("<---my output address");
    Serial.println(); 
    Serial.println(); 
 }


  if (myOutputADDR == FADE) {
    Serial.println ("FADE MODE");
    Serial.print (myOutputADDR, BIN);
    Serial.print ("<---my output address");
     Serial.println(); 
    Serial.println(); 
  }
 

   }

    //CALL THE MODES IF THE ADDRESS MATCHES THE ACTUAL ADDRESS

  if (myOutputADDR == OFF)
  {
    mode_is_off (); // function
  
  }

  if (myOutputADDR == FLASH_TWO)  {
    mode_is_flash_two (); // function
  }

  if (myOutputADDR == FADE) {
    mode_is_fade(); // function
  }

   
}


void mode_is_off () {


 if (myInputADDR == blueDeviceADDR) {
    int devicePin = BLUE;
    digitalWrite (devicePin, LOW);
  }

   if (myInputADDR == greenDeviceADDR) {
    int devicePin = GREEN;
    digitalWrite (devicePin, LOW);
  }

 if (myInputADDR == redDeviceADDR) {
    int devicePin = RED;
    digitalWrite (devicePin, LOW);
  }

 if (myInputADDR == whiteDeviceADDR) {
    int devicePin = WHITE;
    digitalWrite (devicePin, LOW);
  }


}



void mode_is_flash_two () {

 

  if (myInputADDR == greenDeviceADDR | redDeviceADDR) {
    
    digitalWrite (GREEN, HIGH);
     digitalWrite (RED, HIGH);
  
    delay (200);
      digitalWrite (GREEN, LOW);
     digitalWrite (RED, LOW);
 
     
    delay (200);
  }


}

void mode_is_fade() {

  if (myInputADDR == blueDeviceADDR | greenDeviceADDR | redDeviceADDR | whiteDeviceADDR ) {


    for (int i = 0; i < 256; i++) {
      analogWrite (BLUE, i);
      analogWrite (GREEN, i);
      analogWrite (RED, i);
      analogWrite (WHITE, i);
      delay(5);
    }

    for (int i = 255; i >= 0; i--) {
      analogWrite (BLUE, i);
      analogWrite (GREEN, i);
      analogWrite (RED, i);
      analogWrite (WHITE, i);
      delay(5);
    }

  }



}
