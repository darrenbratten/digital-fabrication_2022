
// 4 devices

// This program will let you set three devices to their on/off positions

// For three devices I will have three INPUT addresses
// Their OUTPUTS can be called from the serial monitor by their characters

// 110000 - BLUE LED OFF, called with '0'
// 110100 - BLUE LED ON, called with '4'

// 110001 - GREEN LED OFF, called with '1'
// 110101 - GREEN LED ON, caled with '5'

// 110010 - RED LED OFF, called with '2'
// 110110 - RED LED ON, called with '6'


// STEPS
// 1. Set up LED OUTPUT pins
// 2. Set the pins as OUTPUTs
// 3. Prepare the serial port
// 4. Set up my masks in binary format
// 5. Use Serial.read() to read incoming characters from the monitor
//.6  Establish the lines for the message
// 7. Set up byte return types with the incoming arguments
// 8. Define the three addresses
// 9. Set device pin conditons
// 10. Define the values of the output states
// 11. Set conditions for the device state when it's enabled



// Flash ‰


// OFF 
// BLUE DEV 1   10000000 €
// GREEN DEV 2  10000001
// RED DEV 3    10000010
// WHITE DEV 4  10000011

// ON
// BLUE DEV 1   10000100 „
// GREEN DEV 2  10000101
// RED DEV 3    10000110 
// WHITE DEV 4  10000111

// FLASHING 
// BLUE DEV 1   10001000 ˆ
// GREEN DEV 2  10001001
// RED DEV 3    10011010
// WHITE DEV 4  10001011



// ------------------------------------

// 1. Set up LED OUTPUT pins
#define BLUE 3
#define GREEN 5
#define RED 9
#define WHITE 10

// 8. Define my four addresses
#define inputBlue 0b00000000// 0
#define inputGreen 0b00000001 // 1
#define inputRed 0b00000010 // 10
#define inputWhite 0b00000011 // 11 

// 4. Set up my masks in binary format
byte inputMask =  0b00000011;
byte outputMask = 0b00001100;

// 10. Define the values of the output states
#define ON 1
#define OFF 0
#define FLASH 0b00000010

char incoming;
byte myInput; 
byte myOutput;  


void setup() {

  // 2. Set the pins as OUTPUTs
  pinMode (BLUE, OUTPUT);
  pinMode (GREEN, OUTPUT);
  pinMode (RED, OUTPUT);
  pinMode (WHITE, OUTPUT); 

  // 3. Prepare the serial port
  Serial.begin (9600);


}

void loop() {


  // 5. Use Serial.read() to read incoming characters from the monitor
  while (Serial.available ()) {
    incoming = Serial.read();
    Serial.print ("My input is: ");
    Serial.print (incoming);
    Serial.print ("\t>\t");
    Serial.println(incoming, BIN);
    Serial.println();


    //.6 Establish the lines for the message

    myInput =  getAddress (incoming);
    Serial.print (myInput, BIN);
    Serial.println ("<my Input Address ");

    myOutput = getState (incoming);
    Serial.print (myOutput, BIN);
    Serial.println ("<my Output State");
    Serial.println();

  }


// Set your device outside the serial buffer 
   setDevice (myInput, myOutput);




}

// 7. Set up return types with the incoming arguments

byte getAddress (byte incoming) {
  byte myInput = incoming & inputMask;
  return myInput;
}

byte getState (byte incoming) {
  byte myOutput = (incoming & outputMask) >> 2;
  return myOutput;
}

// 9. Set device pin conditons

void setDevice (byte myInput, byte myOutput) {

  int devicePin;

  if (myInput == inputBlue) {
    devicePin = BLUE;
  }

  if (myInput == inputGreen) {
    devicePin = GREEN; 
  }


  if (myInput == inputRed) {
    devicePin = RED;
  }

  if (myInput == inputWhite) {
    devicePin = WHITE; 
  }



  // 11. Set conditions for the device state when it's enabled

  bool output_state;
  if (myOutput == ON) {
    
    output_state = HIGH; // set it HIGH
    
     digitalWrite(devicePin, output_state);
  }

  if (myOutput == OFF) {
    
    output_state = LOW; // set it LOW
    
     digitalWrite(devicePin, output_state);
  }

  if (myOutput == FLASH) {
    digitalWrite(devicePin, HIGH);
  delay(50);
  digitalWrite(devicePin, LOW);
  delay(50);
  digitalWrite(devicePin, HIGH);
  delay(150);
  digitalWrite(devicePin, LOW);
  delay(150);
  digitalWrite(devicePin, HIGH);
  delay(250);
  digitalWrite(devicePin, LOW);
  delay(250);
  }
  
 


}
