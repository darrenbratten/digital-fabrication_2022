
// Th


#define button 3
#define LED 4

// Keep track of current and last state
boolean lastButton = LOW;
boolean currentButton = LOW;

boolean ledOn = false;




void setup() {


  Serial.begin(9600); // prepare the serial monitor

  pinMode (LED, OUTPUT); 
  pinMode (button, INPUT_PULLUP); 

}

void loop() {

  // Run a debounce function with a boolean return type 
  // it returns lastButton as indeed LOW, or actually HIGH
  // Then, we assign LOW as LOW, or HIGH?
 currentButton = debounce (lastButton);


 // if by LOW, we mean LOW (less than 1.5V present)
 // AND 
 // LOW is currently reversed to be HIGH (3.0V present) 
  if (lastButton == LOW && currentButton == HIGH) {

   // This false boolean reverses to true
    ledOn = !ledOn;
  }


// Set the LED to 1 or 0
 digitalWrite (LED, ledOn) ;


  // LOW should be assigned as LOW or HIGH
  // Stays ON or Stays off 
  lastButton = currentButton;

    
}



boolean debounce (boolean last) {

  // Set up the condition that current means its reading
  boolean current = digitalRead (button);

  // Set our boolean as the opposite - it's not reading
  // but only if the current still isn't reading after the delay
  if (last != current) {
    // delay time to establish if a reading is happening
    delay (5);
    // Define current again after this delay
    current = digitalRead (button);
  }
  // Return TRUE/FALSE into our argument 
  return current;

}
