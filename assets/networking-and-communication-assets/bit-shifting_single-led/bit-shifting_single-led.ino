byte transmit = 2; //define our LED pin

//byte data = 170; //value to transmit, binary 10101010
byte data = 0b101010; // binary value 

//byte mask = 1; //our bitmask
byte bitDelay = 100;

void setup()
{
  pinMode(transmit, OUTPUT);
}

void loop() {


  for (byte mask = 00000001; mask > 0; mask = mask << 1) { //iterate through bit mask
    if (data & mask) { // if bitwise AND resolves to true

      // TRUE LOOKS LIKE THIS:
      // 0b10101010 // our binary value
      // 0b00000010 // Bitmask Position 1...3...5...7
      // 0b00000010 // TRUE - ON

      digitalWrite(transmit, HIGH); // send 1
    }
    else { //if bitwise and resolves to false

      // FALSE LOOKS LIKE THIS
      // 0b10101010
      // 0b00000001 // Bitmask Position 0...2...4...6
      // 0b00000000 // NOT TRUE - OFF

      digitalWrite(transmit, LOW); // send 0
    }
    delay(bitDelay); //delay
  }
}
