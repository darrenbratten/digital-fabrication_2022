
int button = 2;

boolean lastButton = LOW;
boolean currentButton = LOW;
boolean condition = false;

byte ledOffADDR = B00110101;
byte ledOnADDR = B00110001;

int counter = 0;

void setup() {

  pinMode (button, INPUT_PULLUP);

  Serial.begin (9600);


}

void loop() {


  // Define the meaning of 'current' - LOW = LOW?
  currentButton = debounce (lastButton);

  // Within the timeframe of this sketch
  // Has the condition changed?
  if (lastButton == LOW && currentButton == HIGH) {

    condition = !condition; // 0 becomes 1 when something changes

    counter = condition;

  

  }


  lastButton = currentButton;

  

  if (counter == 0)
    {
      Serial.write(ledOnADDR);
    }
    else if (counter == 1)
    {
      Serial.write(ledOffADDR);
    }



}






boolean debounce (boolean last) {


  boolean current = digitalRead (button);
  // i.e. current is returning HIGH

  // if last is opposite and false i.e. returning LOW
  // we know current is reading atfer 5seconds
  // i.e. low is not reading after 5 seconds
  if (last != current) {
    delay (5); // not released
    current = digitalRead (button);
  }

  // return current into last when last is not current
  return current;





}
