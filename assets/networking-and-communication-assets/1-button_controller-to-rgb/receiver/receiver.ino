
char incoming; 

#define LED 4

byte inputBitmask = 0b00000011; 
byte outputBitmask = 0b00001100; 

#define OFF 0b00000001
#define ON 0b00000000

byte myInputADDR;
byte myOutputADDR;


#define greenDeviceADDR 0b00000001 // 1 --->  1 + 5 + 9


void setup() {
  // put your setup code here, to run once:

  Serial.begin (9600); 
  pinMode (LED, OUTPUT); 

}

void loop() {
  // put your main code here, to run repeatedly:

  while (Serial.available ()) {

    incoming = Serial.read(); 

    myInputADDR = (incoming & inputBitmask); 
    myOutputADDR = (incoming & outputBitmask) >> 2;

    
  }

  
 if (myOutputADDR == OFF)
  {
    mode_is_off (); // function
  
  }

  if (myOutputADDR == ON)  {
    mode_is_on (); // function
  }


}

void mode_is_off () {


 if (myInputADDR == greenDeviceADDR) {
    int devicePin = LED;
    digitalWrite (devicePin, LOW);
  }

}

void mode_is_on () {


 if (myInputADDR == greenDeviceADDR) {
    int devicePin = LED;
    digitalWrite (devicePin, HIGH);
  }

}
