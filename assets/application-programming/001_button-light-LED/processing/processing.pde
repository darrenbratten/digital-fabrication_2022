

import processing.serial.*;
Serial myPort; // Calling my Serial port myPort

// Example 5-5: Button as switch
boolean button = false;

int x = 50;
int y = 50;
int w = 100;
int h = 75;

void setup () {

  printArray(Serial.list()); // Print a list of serial ports available 
  myPort = new Serial(this, Serial.list()[1], 9600); // my port is at index 1

  size(480, 270);
}

void draw () {



    if (button) {
      background(255);
      stroke(0);
      myPort.write (1); // Write 1 to turn LED HIGH
    } else {
      background(0);
      stroke(255);
      myPort.write (0); // Write 0 to turn LED LOW
    }
    

    fill(175);
    rect(x, y, w, h);
 

} 


  // When the mouse is pressed, the state of the button is toggled.   
  // Try moving this code to draw() like in the rollover example.  What goes wrong?
  void mousePressed() {
    if (mouseX > x && mouseX < x+w && mouseY > y && mouseY < y+h) {
      button = !button;
    }
  }
