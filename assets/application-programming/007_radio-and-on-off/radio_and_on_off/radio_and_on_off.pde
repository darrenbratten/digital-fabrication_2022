
// This sketch shows how when the button is ON, the selected radio button will print
// When the button is off, the radio button will no longer print


import interfascia.*;


GUIController myInterController; 

GUIController buttonControllerOFF; 
GUIController buttonControllerON; 

IFRadioController myRadioController; 
IFRadioButton myRadio1, myRadio2, myRadio3; 

IFButton myButtonON; 
IFButton myButtonOFF; 


IFLabel myLabel1; 
IFLabel myLabel2; 
IFLabel myLabel3; 

IFTextField myTextField; 

int rX = 25; // radio x
int rY = 480; // radio y 

int lX = 300; // label x
int lY = 550; 

int bX = 250; 
int bY = 477; 

boolean running = false;




void setup () {

  size (364, 756); 



  myInterController = new GUIController (this); // handles all GUI Components 

  myRadioController = new IFRadioController("my radio button group 1"); // the controller handles my group of radio buttons




  // Create GUI components 
  myTextField = new IFTextField("", 30, 10, 300, "Text to be displayed");

  myRadio1 = new  IFRadioButton("Solid", rX, rY, myRadioController);
  myRadio2 = new  IFRadioButton("Slow", rX+75, rY, myRadioController); 
  myRadio3 = new  IFRadioButton("Fast", rX+150, rY, myRadioController); 

  myButtonON = new IFButton("ON", bX, bY);
  myButtonON.setSize(40, 20);
  myButtonOFF = new IFButton("OFF", bX+46, bY);
  myButtonOFF.setSize(40, 20);

  myLabel1 = new IFLabel("255", lX, lY, 18);
  myLabel2 = new IFLabel("255", lX, lY+70, 18);
  myLabel3 = new IFLabel("255", lX, lY+140, 18);



  // Add the components to the controller 
  myInterController.add(myRadio1); 
  myInterController.add(myRadio2); 
  myInterController.add(myRadio3); 
  myInterController.add(myButtonOFF);
  myInterController.add(myButtonON);
  myInterController.add(myLabel1);
  myInterController.add(myLabel2);
  myInterController.add(myLabel3);
  myInterController.add(myTextField);


  //set up action listener 
  myButtonON.addActionListener(this);
  myButtonOFF.addActionListener(this);

  myRadioController.addActionListener(this);
}


void draw () {

  background(200);

  fill(180, 40, 50); 
  ellipse(width/2, 250, width-100, width-100); 


  // 14 * 21 grid 
  for (int x = 0; x <width; x +=width/14) {
    for (int y = 0; y<height; y+=height/21) { 
      stroke (180); 
      noFill (); 
      rect (x, y, width/14, height/21);
    }
  }
}


// Interfascia actionPerformed class initiates events 

void actionPerformed (GUIEvent myEvents) {


  if (myEvents.getSource() == myButtonON)  {
    running = true;
    print(running); 
    print ("---->ON"); 
    print("\n"); 
  } 
    else if (myEvents.getSource() == myButtonOFF) {
    running = false;
    print (running); 
    print ("---->OFF"); 
    print("\n");
  }
  
  if (running == true) {
    
    print(myRadioController.getSelectedIndex());
    if (myRadioController.getSelected() == myRadio1) {
      print ("<----Radio 1"); 
    } 
    if (myRadioController.getSelected() == myRadio2) {
      print ("<----Radio 2"); 
    } 
    if (myRadioController.getSelected() == myRadio3) {
      print ("<----Radio 3"); 
    } 
    
    print("\n");
  }
  
  
}
