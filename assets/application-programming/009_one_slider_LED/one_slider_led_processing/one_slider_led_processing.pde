

import interfascia.*;
import processing.serial.*; // global
Serial myPort;

int convertStr; 


GUIController myInterController; 

// Labels for sliders 
IFLabel myLabel1; 

IFLabel myLabelRED; 
IFProgressBar myProg1; 


int pX = 56;  
int pY = 550; 

int lX = 300; // color numerical label x
int lY = 550; 

int lCx = 25; // text color label x
int lCy = 550; // text color label x

float m0;
String s0; 
float mLabel1; 

void setup () {
  
 
  size (364, 756); 

  printArray(Serial.list()); // printing the list of ports available 
  myPort = new Serial(this, Serial.list()[1], 9600); // selecting the port indexed 1

  myInterController = new GUIController (this); // handles all GUI Components 

  myLabel1 = new IFLabel("", lX, lY, 18);

  myLabelRED = new IFLabel("R", lCx, lCy, 18);

  myProg1 = new IFProgressBar(pX, pY, 204);

  myInterController.add(myLabel1);
  myInterController.add(myLabelRED);
  myInterController.add(myProg1);
}



void draw () {

   background (180); 
  // Labels 
  mLabel1 = map(myProg1.getProgress(), 0, 1, 0, 255);  // 
  int convertInt = int(mLabel1); // convert float to int
  s0 = str(convertInt); // convert int to string
  myLabel1.setLabel(s0); // draw the string
  
  myPort.write(convertStr); 
   
  

}


void mouseDragged () {


  //3 progress bars 
  // if the mouse is pressed and dragged while inside shape
  // map the progress to the position of mouse x, from the start of the shape to the end of the shape

  if (mouseX > myProg1.getX() && mouseX < myProg1.getX() + myProg1.getWidth() && mouseY > myProg1.getY() && mouseY < myProg1.getY() + myProg1.getHeight()) {
    // tuning the map with + and -1
    m0 = map (mouseX, myProg1.getX(), myProg1.getX() + myProg1.getWidth(), 0, 1); 
    myProg1.setProgress(m0);
      convertStr = int(s0); 
    

    
  }
}
