

class Button {

  float x; 
  float y; 
  float h; // height
  float w; // width


  boolean button = false;


  Button (float tempX) {

    x = tempX; 
    y = height/2; 
    w = 100; 
    h = 36;
  }
  


  void display () {

    noStroke(); 

    if (button) { // is true 
      fill (180); 
    } else {
      fill (0);
    }

    rect (x, y, w, h);
  }
  

  // When the mouse is pressed, the state of the button is toggled.   

  void mousePressed() {
    if (mouseX > x && mouseX < x+w && mouseY > y && mouseY < y+h) {
      button = !button;
    }
  }
}
