

class myObject {
  
  float x; 
  float y; 
  float diameter; 
  color myCol; 
  
  myObject (float tempD, color tempC) {
   
    x = width/2; 
    y = height/2; 
    diameter = tempD; 
    myCol = tempC; 
  }
  
  void display () {
    
    noStroke(); 
    fill (myCol); 
    ellipse (x,y,diameter,diameter); 

  }
  
  
}
