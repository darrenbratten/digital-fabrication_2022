
// This sketch imports the Serial library to write serial to Arduino 
// I imported the interfascia library to add radio buttons, a button, text field etc..
// When the ON button is clicked, the radio buttons can write serial
// Each radio button writes a different chracter to enable different modes
// The sketch is incomplete as a whole piece

import interfascia.*;
import processing.serial.*; // global
Serial myPort;

GUIController myInterController; 

GUIController buttonControllerOFF; 
GUIController buttonControllerON; 

IFRadioController myRadioController; 
IFRadioButton myRadio1, myRadio2, myRadio3; 

IFButton myButtonON; 
IFButton myButtonOFF; 

// Labels for sliders 
IFLabel myLabel1; 
IFLabel myLabel2; 
IFLabel myLabel3; 

IFTextField myTextField; 

int rX = 25; // radio x
int rY = 480; // radio y 

int lX = 300; // label x
int lY = 550; 

int bX = 250; 
int bY = 477; 

boolean running = false; // button boolean 

color c;


void setup () {

  size (364, 756); 

  printArray(Serial.list()); // printing the list of ports available 
  myPort = new Serial(this, Serial.list()[1], 9600); // selecting the port indexed 1

  myInterController = new GUIController (this); // handles all GUI Components 

  myRadioController = new IFRadioController("my radio button group 1"); // the controller handles my group of radio buttons


  // Create GUI components 
  myTextField = new IFTextField("", 30, 10, 300, "Text to be displayed");

  myRadio1 = new  IFRadioButton("Solid", rX, rY, myRadioController);
  myRadio2 = new  IFRadioButton("1 by 1", rX+75, rY, myRadioController); 
  myRadio3 = new  IFRadioButton("Blink", rX+150, rY, myRadioController); 

  myButtonON = new IFButton("ON", bX, bY);
  myButtonON.setSize(40, 20);
  myButtonOFF = new IFButton("OFF", bX+46, bY);
  myButtonOFF.setSize(40, 20);

  myLabel1 = new IFLabel("255", lX, lY, 18);
  myLabel2 = new IFLabel("255", lX, lY+70, 18);
  myLabel3 = new IFLabel("255", lX, lY+140, 18);

  // Add the components to the controller 
  myInterController.add(myRadio1); 
  myInterController.add(myRadio2); 
  myInterController.add(myRadio3); 
  myInterController.add(myButtonOFF);
  myInterController.add(myButtonON);
  myInterController.add(myLabel1);
  myInterController.add(myLabel2);
  myInterController.add(myLabel3);
  myInterController.add(myTextField);


  //set up action listener 
  myButtonON.addActionListener(this);
  myButtonOFF.addActionListener(this);

// The action listener is the radio controller here, not the button
  myRadioController.addActionListener(this);
}


void draw () {

  background(200);
  
  
  // 14 * 21 grid 
  for (int x = 0; x <width; x +=width/14) {
    for (int y = 0; y<height; y+=height/21) { 
      stroke (180); 
      noFill (); 
      rect (x, y, width/14, height/21);
    }
  }

noStroke(); 

  if (running == true) {
    fill(c);
  } else {
    noFill();
  }

  ellipse(width/2, 250, width-100, width-100); 


}



void actionPerformed (GUIEvent myEvents) {

  if (myEvents.getSource() == myButtonON) { // when clicked 
    running = true;
    print(running); 
    print ("---->ON"); 
    print("\n");
  } else if (myEvents.getSource() == myButtonOFF) {
    running = false;
    print (running); 
    print ("---->OFF"); 
    print("\n");
    myPort.write('0');
  }

  if (running == true) { // setting up the radio buttons to print and write 

    print(myRadioController.getSelectedIndex());
    if (myRadioController.getSelected() == myRadio1) {
      print ("<----Radio 1"); 

      c = 30; 
      myPort.write('1');
      delay(5);
    } 
    if (myRadioController.getSelected() == myRadio2) {
      print ("<----Radio 2"); 
      myPort.write('2');
      c = 80; 
      delay(5);
    } 
    if (myRadioController.getSelected() == myRadio3) {
      print ("<----Radio 3"); 
      myPort.write('3');
      fill (180); 
      c = 180; 
      delay(5);
    } 

    print("\n");
  }
}
