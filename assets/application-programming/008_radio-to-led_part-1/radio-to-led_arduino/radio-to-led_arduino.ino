
// This sketch reads incoming serial data and lights 3 leds to different modes depending on what it reads


char a; // value to store incoming

#define ledA 9
#define ledB 10
#define ledC 11

void setup() {
  // put your setup code here, to run once:

  Serial.begin (9600);

  pinMode(ledA, OUTPUT);
  pinMode(ledB, OUTPUT);
  pinMode(ledC, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:


  while (Serial.available()) {

    a = Serial.read();

  }

  if (a == '0'); {
    digitalWrite (ledA, LOW);
    digitalWrite (ledB, LOW);
    digitalWrite (ledC, LOW);
  }

  if (a == '1') {
    digitalWrite(ledA, HIGH);
    digitalWrite(ledB, HIGH);
    digitalWrite(ledC, HIGH);

  }
  if (a == '2') {
    digitalWrite(ledA, HIGH);
    delay(250);
    digitalWrite(ledA, LOW);

    digitalWrite(ledB, HIGH);
    delay(250);
    digitalWrite(ledB, LOW);
    delay(250);
    digitalWrite(ledC, HIGH);
    delay(250);
    digitalWrite(ledC, LOW);
    delay(250);
  }
  if (a == '3') {
    digitalWrite(ledA, HIGH);
    digitalWrite(ledB, HIGH);
    digitalWrite(ledC, HIGH);
    delay(500);
    digitalWrite(ledA, LOW);
    digitalWrite(ledB, LOW);
    digitalWrite(ledC, LOW);
    delay(250);

  }


}
