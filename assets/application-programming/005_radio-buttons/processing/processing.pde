Button buttonA; // create my class name
Button buttonB; 
Button buttonC; 


import processing.serial.*;
Serial myPort; // Calling my Serial port myPort




void setup () {

  background(255); 
  size (600, 100); 

  printArray(Serial.list()); // Print a list of serial ports available 
  myPort = new Serial(this, Serial.list()[1], 9600); // my port is at index 1

  buttonA = new Button (20); 
  buttonB = new Button (width/2-50); 
  buttonC = new Button (width-150 + 30);
}

void draw () {

  buttonA.display(); 
  buttonB.display(); 
  buttonC.display(); 


}


void mousePressed () {

  buttonA.mousePressed(); 
  buttonB.mousePressed();
  buttonC.mousePressed();
}
