

class Button {

  float x; 
  float y; 
  float h; // height
  float w; // width
  color col; 


  boolean button = false;

  color solidFill; 



  Button (float tempX ) {

    x = tempX; 
    y = height/2; 
    w = 100; 
    h = 36;
    col = 180;
  }

  void display () {

    stroke(0);   

    if (button) {
      
      
      fill(solidFill);
    }

    rect (x, y, w, h);
  }




  // When the mouse is pressed, the state of the button is toggled.   

  void mousePressed() {

    if (mouseX > x && mouseX < x+w && mouseY > y && mouseY < y+h) {
      button = !button; // true
    }
  }
}
