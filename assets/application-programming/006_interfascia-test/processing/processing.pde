

import interfascia.*;


GUIController myInterController;  

IFRadioController myRadioController; 
IFRadioButton myRadio1, myRadio2, myRadio3; 

IFCheckBox myCheckBox1, myCheckBox2;

IFButton myButton; 

IFTextField myTextField; 

IFProgressBar myProgressBar; 

IFLabel myLabel; 


void setup () {

  size (600, 600); 


  myInterController = new GUIController (this); // handles all GUI Components 

  myRadioController = new IFRadioController("my radio button group"); // the controller handles my group of radio buttons




// Create GUI components 

  myRadio1 = new  IFRadioButton("Radio 1", 30, 30, myRadioController);
  myRadio2 = new  IFRadioButton("Radio 2", 30, 60, myRadioController); 
  myRadio3 = new  IFRadioButton("Radio 3", 30, 90, myRadioController); 
  myCheckBox1 = new IFCheckBox("Enable", 30, 120); // create a checkbox
  myCheckBox2 = new IFCheckBox("Enable", 30, 150); // create a checkbox
  myButton = new IFButton("Click Me", 30, 180);
  myTextField = new IFTextField("myLabel", 30, 210, 300, "Text to be displayed");
  myProgressBar = new IFProgressBar(30, 240, 300);
  myLabel = new IFLabel("Display me", 30, 270, 18);
  


  // Add the components to the controller 
  myInterController.add(myRadio1); 
  myInterController.add(myRadio2); 
  myInterController.add(myRadio3); 
  myInterController.add(myCheckBox1); 
  myInterController.add(myCheckBox2); 
  myInterController.add(myButton);
  myInterController.add(myTextField);
  myInterController.add(myProgressBar);
  myInterController.add(myLabel);
  
  
  
}


void draw () {

  background(200);
  
  
  
}
