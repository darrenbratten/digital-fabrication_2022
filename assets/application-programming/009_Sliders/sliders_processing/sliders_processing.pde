
// This sketch imports the Serial library to write serial to Arduino 
// I imported the interfascia library to add radio buttons, a button, text field etc..
// When the ON button is clicked, the radio buttons can write serial
// Each radio button writes a different chracter to enable different modes
// There are 3 labels and 3 progress bars/sliders which are mapped to update on mouse drag
// The sketch is incomplete as a whole piece


import interfascia.*;
import processing.serial.*; // global
Serial myPort;

GUIController myInterController; 

GUIController buttonControllerOFF; 
GUIController buttonControllerON; 

IFRadioController myRadioController; 
IFRadioButton myRadio1, myRadio2, myRadio3; 

IFButton myButtonON; 
IFButton myButtonOFF; 

// Labels for sliders 
IFLabel myLabel1; 
IFLabel myLabel2; 
IFLabel myLabel3; 
IFLabel myLabelRED; 
IFLabel myLabelGREEN; 
IFLabel myLabelBLUE; 

IFTextField myTextField; 

int rX = 25; // radio x
int rY = 480; // radio y 

int lX = 300; // color numerical label x
int lY = 550; 

int lCx = 25; // text color label x
int lCy = 550; // text color label x

int bX = 250; 
int bY = 477; 

int pX = 56;  
int pY = 550; 

// progress bar 
IFProgressBar myProg1; 
IFProgressBar myProg2; 
IFProgressBar myProg3; 

float m0; 
float m1; 
float m2; 
String s0; 
String s1; 
String s2; 
float mLabel1; 
float mLabel2; 
float mLabel3; 

String sR; // radio string 
String sRC; // radio controller string

String sBtn; // button string
String waiting; 

boolean running = false; // button boolean 

boolean off = false;


void setup () {

  size (364, 756); 

  printArray(Serial.list()); // printing the list of ports available 
  myPort = new Serial(this, Serial.list()[1], 9600); // selecting the port indexed 1

  myInterController = new GUIController (this); // handles all GUI Components 

  myRadioController = new IFRadioController("my radio button group 1"); // the controller handles my group of radio buttons


  // Create GUI components 
  myTextField = new IFTextField("", 30, 10, 300, "Text to be displayed");

  myRadio1 = new  IFRadioButton("Solid", rX, rY, myRadioController);
  myRadio2 = new  IFRadioButton("1 by 1", rX+75, rY, myRadioController); 
  myRadio3 = new  IFRadioButton("Blink", rX+150, rY, myRadioController); 

  myButtonON = new IFButton("ON", bX, bY);
  myButtonON.setSize(40, 20);
  myButtonOFF = new IFButton("OFF", bX+46, bY);
  myButtonOFF.setSize(40, 20);

  myLabel1 = new IFLabel("", lX, lY, 18);
  myLabel2 = new IFLabel("", lX, lY+70, 18);
  myLabel3 = new IFLabel("", lX, lY+140, 18);
  
   myLabelRED = new IFLabel("R", lCx, lCy, 18);
  myLabelGREEN = new IFLabel("G", lCx, lCy+70, 18);
  myLabelBLUE = new IFLabel("B", lCx, lCy+140, 18);

  myProg1 = new IFProgressBar(pX, pY, 204);
  myProg2 = new IFProgressBar(pX, pY+70, 204);
  myProg3 = new IFProgressBar(pX, pY +140, 204);

  // Add the components to the controller 
  myInterController.add(myRadio1); 
  myInterController.add(myRadio2); 
  myInterController.add(myRadio3); 
  myInterController.add(myButtonOFF);
  myInterController.add(myButtonON);
  myInterController.add(myLabel1);
  myInterController.add(myLabel2);
  myInterController.add(myLabel3);
    myInterController.add(myLabelRED);
  myInterController.add(myLabelGREEN);
  myInterController.add(myLabelBLUE);
  myInterController.add(myTextField);
  myInterController.add(myProg1); 
  myInterController.add(myProg2); 
  myInterController.add(myProg3); 


  //set up action listener 
  myButtonON.addActionListener(this);
  myButtonOFF.addActionListener(this);

  // The action listener is the radio controller here, not the button
  myRadioController.addActionListener(this);
  
}


void draw () {

  background(200);
   
  


  // 14 * 21 grid 
  for (int x = 0; x <width; x +=width/14) {
    for (int y = 0; y<height; y+=height/21) { 
      stroke (180); 
      noFill (); 
      rect (x, y, width/14, height/21);
    }
  }



  // Labels 
  mLabel1 = map(myProg1.getProgress(), 0, 1, 0, 255);  // 
  int convertInt = int(mLabel1); // convert float to int
  s0 = str(convertInt); // convert int to string
  myLabel1.setLabel(s0); // draw the string


  mLabel2 = map(myProg2.getProgress(), 0, 1, 0, 255);  // 
  convertInt = int(mLabel2); // convert float to int
  s1 = str(convertInt); // convert int to string
  myLabel2.setLabel(s1); // draw the string

  mLabel3 = map(myProg3.getProgress(), 0, 1, 0, 255);  // 
  convertInt = int(mLabel3); // convert float to int
  s2 = str(convertInt); // convert int to string
  myLabel3.setLabel(s2); // draw the string


// print to the terminal



if (running == true) {
  print ("R = "); // RED
  print (s0);
  print (","); 
  print ("G = "); // GREEN
  print (s1);
  print (","); 
  print ("B = "); // BLUE
  print (s2);
  print ("\t"); 
  print (sRC); // Radio Controller index
  println(sR); // which radio 
} else if (running == false) {
   println (sBtn); 
} 


// convert string back to float to colour ellipse
float s0c = float(s0); 
float s1c = float (s1); 
float s2c = float (s2); 

  noStroke();
  
  if (running == true) {
    fill(s0c, s1c, s2c); // use float values from string
  } else {
    noFill();
  }

  ellipse(width/2, 250, width-100, width-100); 

  
}


// this function handles keyboard events 
void actionPerformed (GUIEvent myEvents) {

  
  // if the ON button is pressed, it's true
  if (myEvents.getSource() == myButtonON) { // when clicked 
    running = true;
    // print(running); 
    // print ("---->ON"); 
    // print("\n");
    
    // otherwise if my OFF button is pressed, say false
  } else if (myEvents.getSource() == myButtonOFF) {
    running = false;
    // print (running); 
    //print ("---->OFF"); 
    //print("\n");
    sBtn = "Device is turned off"; 
    myPort.write('0');
  } 



  if (running == true) { // setting up the radio buttons to print and write 
    
    sRC = str(myRadioController.getSelectedIndex()); 


    if (myRadioController.getSelected() == myRadio1) {

      sR = "<----Radio 0"; // radio string


      myPort.write('1'); 
    }


    if (myRadioController.getSelected() == myRadio2) {

      sR = "<----Radio 1"; // radio string

      myPort.write('2');
 
    } 

    if (myRadioController.getSelected() == myRadio3) {

      sR = "<----Radio 2"; // radio string

      myPort.write('3');
   

    }
  }

  print("\n"); // print a newline
}



void mouseDragged () {


  //3 progress bars 
  // if the mouse is pressed and dragged while inside shape
  // map the progress to the position of mouse x, from the start of the shape to the end of the shape

  if (mouseX > myProg1.getX() && mouseX < myProg1.getX() + myProg1.getWidth() && mouseY > myProg1.getY() && mouseY < myProg1.getY() + myProg1.getHeight()) {
    // tuning the map with + and -1
    m0 = map (mouseX, myProg1.getX(), myProg1.getX() + myProg1.getWidth(), 0, 1); 
    myProg1.setProgress(m0);
  } 


  if (mouseX > myProg2.getX() && mouseX < myProg2.getX() + myProg2.getWidth() && mouseY > myProg2.getY() && mouseY < myProg2.getY() + myProg2.getHeight()) {
    // tuning the map with + and -1
    m1 = map (mouseX, myProg2.getX()+1, myProg2.getX() + myProg2.getWidth()-1, 0, 1); 
    myProg2.setProgress(m1);
  }

  if (mouseX > myProg3.getX() && mouseX < myProg3.getX() + myProg3.getWidth() && mouseY > myProg3.getY() && mouseY < myProg3.getY() + myProg3.getHeight()) {
    // tuning the map with + and -1
    m2 = map (mouseX, myProg3.getX()+1, myProg3.getX() + myProg3.getWidth()-1, 0, 1); 
    myProg3.setProgress(m2);
  }
}
