
import interfascia.*;
import processing.serial.*; // global
Serial myPort;

GUIController myInterController; 

IFButton myButtonON; 
IFButton myButtonOFF; 

// Labels for sliders 
IFLabel myLabel1; 
IFLabel myLabel2; 
IFLabel myLabel3; 
IFLabel myLabelRED; 
IFLabel myLabelGREEN; 
IFLabel myLabelBLUE; 

IFTextField myTextField; 

int lX = 300; // color numerical label x
int lY = 550; 

int lCx = 25; // text color label x
int lCy = 550; // text color label x

int pX = 56;  
int pY = 550; 

// progress bar 
IFProgressBar myProg1; 
IFProgressBar myProg2; 
IFProgressBar myProg3; 

float m0; 
float m1; 
float m2; 
String s0; 
String s1; 
String s2; 
float mLabel1; 
float mLabel2; 
float mLabel3; 

String sBtn; // button string
String waiting; 


String sBytes; 


void setup () {

  size (364, 756); 

  printArray(Serial.list()); // printing the list of ports available 
  myPort = new Serial(this, Serial.list()[1], 9600); // selecting the port indexed 1

  myInterController = new GUIController (this); // handles all GUI Components 

  // Create GUI components 
  myTextField = new IFTextField("", 30, 10, 300, "Text to be displayed");


  myLabel1 = new IFLabel("", lX, lY, 18);
  myLabel2 = new IFLabel("", lX, lY+70, 18);
  myLabel3 = new IFLabel("", lX, lY+140, 18);

  myLabelRED = new IFLabel("R", lCx, lCy, 18);
  myLabelGREEN = new IFLabel("G", lCx, lCy+70, 18);
  myLabelBLUE = new IFLabel("B", lCx, lCy+140, 18);

  myProg1 = new IFProgressBar(pX, pY, 204);
  myProg2 = new IFProgressBar(pX, pY+70, 204);
  myProg3 = new IFProgressBar(pX, pY +140, 204);

  // Add the components to the controller 
  myInterController.add(myLabel1);
  myInterController.add(myLabel2);
  myInterController.add(myLabel3);
  myInterController.add(myLabelRED);
  myInterController.add(myLabelGREEN);
  myInterController.add(myLabelBLUE);
  myInterController.add(myTextField);
  myInterController.add(myProg1); 
  myInterController.add(myProg2); 
  myInterController.add(myProg3);
}


void draw () {

  background(200);


colorMode(HSB, 360, 100, 100); 

  // 14 * 21 grid 
  for (int x = 0; x <width; x +=width/14) {
    for (int y = 0; y<height; y+=height/21) { 
      stroke (180); 
      noFill (); 
      rect (x, y, width/14, height/21);
    }
  }


  // Labels 
  mLabel1 = map(myProg1.getProgress(), 0, 1, 0, 360);  // 
  int convertInt = int(mLabel1); // convert float to int
  s0 = str(convertInt); // convert int to string
  myLabel1.setLabel(s0); // draw the string


  mLabel2 = map(myProg2.getProgress(), 0, 1, 0, 100);  // 
  convertInt = int(mLabel2); // convert float to int
  s1 = str(convertInt); // convert int to string
  myLabel2.setLabel(s1); // draw the string

  mLabel3 = map(myProg3.getProgress(), 0, 1, 0, 100);  // 
  convertInt = int(mLabel3); // convert float to int
  s2 = str(convertInt); // convert int to string
  myLabel3.setLabel(s2); // draw the string


  // print to the terminal


  // convert string back to float to colour ellipse
  float s0c = float(s0); 
  float s1c = float (s1); 
  float s2c = float (s2); 

  noStroke();



  fill( s0c, s1c, s2c); // use float values from string

  ellipse(width/2, 250, width-100, width-100);
}



void mouseDragged () {


  //print ("R = "); // RED
  //print (s0);
  //print (","); 
  //print ("G = "); // GREEN
  //print (s1);
  //print (","); 
  //print ("B = "); // BLUE
  //print (s2);
  //print ("\n"); 



  sBytes = s0 + "," + s1 + "," + s2 + "\n"; 

  myPort.write (sBytes); 


  print (s0); 
  print (","); 
  print (s1); 
  print (","); 
  print (s2);
  print ("\n");






  if (mouseX > myProg1.getX() && mouseX < myProg1.getX() + myProg1.getWidth() && mouseY > myProg1.getY() && mouseY < myProg1.getY() + myProg1.getHeight()) {
    // tuning the map with + and -1
    m0 = map (mouseX, myProg1.getX()+1, myProg1.getX() + myProg1.getWidth()-1, 0, 1); 
    myProg1.setProgress(m0);

  } 


  if (mouseX > myProg2.getX() && mouseX < myProg2.getX() + myProg2.getWidth() && mouseY > myProg2.getY() && mouseY < myProg2.getY() + myProg2.getHeight()) {
    // tuning the map with + and -1
    m1 = map (mouseX, myProg2.getX()+1, myProg2.getX() + myProg2.getWidth()-1, 0, 1); 
    myProg2.setProgress(m1);
  }

  if (mouseX > myProg3.getX() && mouseX < myProg3.getX() + myProg3.getWidth() && mouseY > myProg3.getY() && mouseY < myProg3.getY() + myProg3.getHeight()) {
    // tuning the map with + and -1
    m2 = map (mouseX, myProg3.getX()+1, myProg3.getX() + myProg3.getWidth()-1, 0, 1); 
    myProg3.setProgress(m2);
  }
}
