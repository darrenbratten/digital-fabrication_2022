// CharliePlexing 12 LEDs
// Tony Goodhew 27 June 2020

#define b 7   // Blue wire
#define y 6   // Yellow wire
#define g 5   // Green wire
#define r 4   // red wire
#define potPin A0  // 10 K Ohm potentiometer on A0

// pin numbers    0,1,2,3,4,5,6,7,8,9,10,11
byte anodes[]  = {b,y,y,g,g,r,r,b,y,r, b, g};
byte caths[]   = {y,b,g,y,r,g,b,r,r,y, g, b};
byte input1[]  = {g,g,b,b,b,b,y,y,b,b, r, r};
byte input2[]  = {r,r,r,r,y,y,g,g,g,g, y, y};

void show(int p) {  // Turn on a single LED ( p = 0...5)
  pinMode(input1[p], INPUT);     // Disconnect
  pinMode(input2[p], INPUT);
  pinMode(caths[p], OUTPUT);
  digitalWrite(caths[p], 0);   // GND
  pinMode(anodes[p], OUTPUT);
  digitalWrite(anodes[p], 1);  // 5V
}

void show2(int p) {
  for (int m = 0; m < p; m++)  {
    show(m);
    delay(50);  // Reduced wait time to remove flicker
  }
}

void setup() {
  Serial.begin(115200);  // Ready for de-bugging  
}

void loop() {
  int potValue = analogRead(potPin); // Read potentiometer
  int pointer = potValue / 80; // values 0...12
  if (pointer == 0) {  
    pinMode(b, INPUT);  // Turn off all LEDs
    pinMode(y, INPUT);  // Disconnect all wires

    pinMode(g, INPUT);
    pinMode(r, INPUT);
  }
  else{
    show2(pointer);     // Bar graph display
  } 
}
