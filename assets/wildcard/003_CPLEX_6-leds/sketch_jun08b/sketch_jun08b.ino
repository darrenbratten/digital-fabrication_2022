
// Code from Liz Sanders, Controlling LEDs with Arduino book
// I have just rearranged the code, but I did not write it

#define pin2 13
#define pin3 8
#define pin4 2


void setup () {

}


void loop () {
  led1();
  delay(50);
  led3();
  delay(50);
  led5();
  delay(50);
  led2();
  delay(50);
  led4();
  delay(50);
  led6();
  delay(50);

}


void led1 () {
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);
  pinMode(pin4, INPUT); // disabled
  digitalWrite (pin2, LOW);
  digitalWrite (pin3, HIGH);
}

void led2 () {
  pinMode(pin2, OUTPUT);
  pinMode(pin3, OUTPUT);
  pinMode(pin4, INPUT); // disabled
  digitalWrite (pin2, HIGH);
  digitalWrite (pin3, LOW);
}

void led3 () {
  pinMode(pin2, INPUT); // disabled
  pinMode(pin3, OUTPUT);
  pinMode(pin4, OUTPUT);
  digitalWrite (pin3, LOW);
  digitalWrite (pin4, HIGH);
}

void led4 () {
  pinMode(pin2, INPUT); // disabled
  pinMode(pin3, OUTPUT);
  pinMode(pin4, OUTPUT);
  digitalWrite (pin3, HIGH);
  digitalWrite (pin4, LOW);
}

void led5 () {
  pinMode(pin2, OUTPUT);
  pinMode(pin3, INPUT); // disabled
  pinMode(pin4, OUTPUT);
  digitalWrite (pin2, HIGH);
  digitalWrite (pin4, LOW);
}

void led6 () {
  pinMode(pin2, OUTPUT);
  pinMode(pin3, INPUT); // disabled
  pinMode(pin4, OUTPUT);
  digitalWrite (pin2, LOW);
  digitalWrite (pin4, HIGH);
}
