

#define hallSensor 3   //Sensor on pin 3

float val = 0;  //A variable to store the incoming value


void setup() {

  // Preparing the serial monitor with 9600 baud rate 
  Serial.begin(9600);

  // Marking my sensor as an input device
  pinMode(hallSensor,INPUT);
 

}

void loop() {


   val=analogRead(hallSensor);  // Store the analog reading in "val" 
   Serial.println(val); // print it 
   delay (50);  // delay it so as not to flood the monitor 
}
