
// Repeat every character - 'echo'

char data; // holds incoming character 

void setup() {
  // put your setup code here, to run once:

  Serial.begin (9600); // Prepare the serial port at 9600 baud rate 
}

void loop() {

  if (Serial.available() > 0) // Only print when data is availble/received

  {
    data = Serial.read(); // read byte of data
    Serial.print (data); // printing data byte 
  }

}
