
//Processing sketch to read data and change colour on the screen 

// Import and inisitalise serial port library 
import processing.serial.*; 
Serial port; 

float val = 0; // for holding the value from the potentiometer 

void setup () {

  size (400, 800); 
  port = new Serial (this, "/dev/cu.usbmodem14101", 9600); // setup the correct Serial port
  port.buffer('\n'); // set up port to read until newline
}

void draw () {

  background (0, 0, val); // vary the brightness from 0-255 
  ellipse(width/2, height/2, val, val); // position in the middle, and scale it from 0-255
}

void serialEvent (Serial port) 
{
  val = float(port.readStringUntil ('\n')); // gets value
}
