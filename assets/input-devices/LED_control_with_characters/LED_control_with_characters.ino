

#define LED 4

char data; // to hold the incoming value


void setup() {
  // put your setup code here, to run once:

  Serial.begin (9600); 
  pinMode (LED, OUTPUT); 

}

void loop() {
  // put your main code here, to run repeatedly:

  // only act when data is available in the buffer
  if (Serial.available () > 0)

  {
    data = Serial.read(); // read the byte 
    // Turn on the LED 
    if (data == '5')
    {
      digitalWrite (LED, LOW); 
      Serial.println ("LED ON"); 
    }
    // TURN OFF LED
    else if (data == '6')
    {
      digitalWrite (LED, HIGH); 
      Serial.println ("LED OFF"); 
    }
    
     
    }
  }
