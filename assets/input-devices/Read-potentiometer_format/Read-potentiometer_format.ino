
// This sketch will read a potentiometer 
// and print its raw and percentage values to the serial monitor

const int pot = 2; // Setting up potentiometer on analog pin 0

void setup() {
  
Serial.begin (9600); // Start serial port with this baud rate

}

void loop() {


// First, print this header with tabs start on a new line
Serial.println ("\nAnalog Pin\tRaw Value\tPercentage"); 
Serial.println ("------------------------------------");

// Exit this loop after every 10 lines 
for (int i = 0; i < 10; i++) {
 
  int val = analogRead (pot); // Read the potentiometer 
  int per = map (val, 0, 1023, 0, 100); // convert to percentage


  Serial.print ("A0"); 
    Serial.print ("\t\t"); 
  Serial.print (val);
  Serial.print ("\t\t"); 
  Serial.print (per);
  Serial.print ("%"); 
  delay(250); // Wait for a 1/4 second 
  Serial.println (""); 

}
  
  

}
