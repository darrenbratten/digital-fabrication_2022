
// This sketch will read a potentiometer 
// and print its raw and percentage values to the serial monitor

const int pot = 2; // Setting up potentiometer on analog pin 0

int val; 

void setup() {
  
Serial.begin (9600); // Start serial port with this baud rate


}

void loop() {

  val = map ( analogRead (pot), 0, 1023, 0, 255); // Read the potentiometer 
  Serial.println (val); 
  delay (50); // Delay so not to flood the buffer


}
  
  
