const int LED = 4;
const int button = 3;
const int pot = 2; 
int val = 0; // to hold analog reading 

int output;

void setup() {
  // put your setup code here, to run once:

Serial.begin(115200);
  pinMode(LED, OUTPUT); 
  pinMode (button, INPUT_PULLUP); 

}
void loop() {
  // put your main code here, to run repeatedly:


if (digitalRead (button) == LOW) {
  digitalWrite(4, HIGH);
  delay(50);
  digitalWrite(4, LOW);
  delay(50);
  digitalWrite(4, HIGH);
  delay(50);
  digitalWrite(4, LOW);
  delay(25);
}
else 
{ val = analogRead (pot); 

output = map(val, 0,1023,0,255); 
analogWrite (LED,output); 
}

}
